# GrInjector <br> [![Maven Central](https://img.shields.io/maven-central/v/com.github.henryco/grinjector.svg)](http://repo1.maven.org/maven2/com/github/henryco/grinjector/) [![GitHub license](https://img.shields.io/badge/license-MIT-yellow.svg)](https://raw.githubusercontent.com/henryco/GrInjector/master/LICENSE) 
###### Lightweight reflective dependency injector for java and Android.
###### <a href="https://github.com/henryco/GrInjector/wiki/GrInjector-guide.">**Wiki page**</a>

<br>

## Installation 
You need to add maven central repository into your project and then add dependency: <br><br>
<b>Gradle</b>

```Groovy
compile 'com.github.henryco:grinjector:1.1.0'
```

<b>Maven</b>

```XML
<dependency>
    <groupId>com.github.henryco</groupId>
    <artifactId>grinjector</artifactId>
    <version>1.1.0</version>
</dependency>
```

<br>

#### TODO: 
* JSR-330 Compatibility
* Scopes
* Refactoring
