package com.github.henryco.injector.injector.component;

import com.github.henryco.injector.meta.annotations.Component;
import com.github.henryco.injector.meta.annotations.Singleton;

/**
 * @author Henry on 17/12/17.
 */
@Component
@Singleton
public class ComponentC {

}